import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogInPage {

    private final WebDriver driver;

    @FindBy(how = How.XPATH, using = "//*[@class='panel-body login-panel-body']/child::button")
    public WebElement logInButton;

    public LogInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        //task5
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(logInButton));
    }

    public SingInPage singInPage() {
        logInButton.click();
        return new SingInPage(driver);
    }
}
